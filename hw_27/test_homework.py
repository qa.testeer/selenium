import pytest
from selenium.webdriver.common.by import By
import time


@pytest.mark.usefixtures("chrome_driver_init")
class TestUrl:

    def accept_cookies(self):
        accept_cookies_button_element = self.driver.find_element(by=By.ID, value="accept-choices")
        accept_cookies_button_element.click()
        time.sleep(5)

    def open_tutorials(self):
        open_tutorials = self.driver.find_element(by=By.XPATH,  value="//*[@title='Tutorials']")
        open_tutorials.click()
        time.sleep(5)

    def choose_learn_sql(self):
        learn_sql = self.driver.find_element(by=By.XPATH,  value="//div[@class='w3-row-padding w3-bar-block']//a[text()='Learn SQL']")
        learn_sql.click()
        time.sleep(5)

    def steps(self):
        self.accept_cookies()
        self.open_tutorials()
        self.choose_learn_sql()

    def test_title(self):
        self.steps()
        title = "SQL Tutorial"
        assert title == self.driver.title

    def test_url(self):
        self.steps()
        url = "https://www.w3schools.com/sql/default.asp"
        assert url == self.driver.current_url



