import pytest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Chrome


@pytest.fixture()
def chrome_driver_init(request):
    service = Service("./chromedriver")
    options = Options()
    options.add_argument("start-maximized")
    driver = Chrome(service=service, options=options)
    request.cls.driver = driver
    driver.get("https://www.w3schools.com/")
    yield driver
    driver.quit()